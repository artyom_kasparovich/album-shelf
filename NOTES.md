# Notes

## Acceptance Criteria

> The app tested in Chrome browser.

- [ ] Create a new interface for browsing a collection of albums as per the attached design artefacts.
  - [ ] Album artwork is rendered in a square format.
  - [ ] The typeface is "Manrope" - [it's available on Google Fonts](https://fonts.google.com/specimen/Manrope)
- [ ] Data for the interface is loaded via the API provided in the Github repo
- [ ] Sorting of the albums is possible by the album name, release date and total tracks
  > There is collision with design mockups. So acceptance criteria have highest priority
- [ ] When choosing a sorting variable, the sorting section collapses automatically
- [ ] The interface scales nicely from a 320px wide viewport up to 1200px
  > There is collision with design mockups (starts from 375px) but implementation supports 320px. Also introduced one more breakpoint (768px) that will transition to tablet view. After 1200px the app is center aligned.
- [ ] It is possible to browse through the whole catalogue of albums
  > Decided to load new content by pressing loading button
- [ ] Once you're happy to share your work, please write the 'release notes' for this feature. The format here is free form.

### Bonus

- [] Filtering by text input search.
  > Introduced debounce for requests
- [] Filtering by artist.
  - [] filtering by only those artists who have two or more albums.
    > New searching query from `SearchInput` will reset artist's selection and vice-verse

## Comments

In the app I left `Note::` labels for sharing thoughts. In the task I'm not using state managers (like `redux`) and data fetching libraries (like `swr`) purposely.

### TODO. In real app it's nice to have the next things:

- API error handling
- App error handling
- Routing
- Internationalization
- Testing (testing)
- Commit linter, change log generation, pre-commit hooks (linting, type checking, prettier check, running tests)

## Release Notes

1.0.0

### Modules

- Add `App` module. Module serves for bootstraping and running the app. Contains shared code.
- Add `Artists` module. Module contains artist's related logic.
- Add `Albums` module. Module contains artist's related logic.
- Add `AppTypes` module. Global app type system.

### Features

- SVGSprite for working with icons
- Theme and design tokens
- Config
- Album's list with infinity loading
- Album's sorting
- Album's filtering by artist
- Album's filtering by search text
