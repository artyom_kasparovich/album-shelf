import { config } from './config';
import { svgWrapper } from './svgSprite';
import { initApp } from './App/index';

document.body.appendChild(svgWrapper);

initApp(config);

/* add release notes, add thoughts about what to add */
