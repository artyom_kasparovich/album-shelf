import AppTypes from 'AppTypes';

export type Album = {
  artists: AppTypes.Entities.Artist[];
  cover: string;
  id: string;
  name: string;
  releaseDate: string;
  totalTracks: number;
};
