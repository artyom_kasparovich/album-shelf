import { BaseApiConstructorProps } from 'App/api/Base';
import { List, ListRequestParams } from 'App/api/List';
import { Album } from './types';

export class Albums extends List<Album> {
  private url: string;

  constructor(props: BaseApiConstructorProps) {
    super(props);
    this.url = '/albums';
  }

  listPaginated(params: ListRequestParams) {
    return this.list(this.url, params);
  }
}
