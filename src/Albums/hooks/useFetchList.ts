import { useMemo } from 'react';
import { useFetch, useAPIInitialization } from 'App/hooks';
import { Albums } from '../api';

export const useFetchList = (page: number, query: string, sorting: string) => {
  const albums = useAPIInitialization(Albums);

  const fetch = useMemo(() => () => albums.listPaginated({ page, query, sorting }), [page, query, sorting, albums]);
  const response = useFetch(fetch, true);

  return response;
};
