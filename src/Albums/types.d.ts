import * as Models from './api/types';

declare module 'AppTypes' {
  export namespace Entities {
    export type Album = Models.Album;
  }
}
