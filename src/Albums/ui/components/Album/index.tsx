import React from 'react';
import { Icon, IconType, IconShade } from 'App/ui/components/Icon';
import { Wrapper, Image, Title, Meta, MetaDot, ImageWrapper, PlayIcon } from './styles';
import { Props } from './types';

// Note:: here we can introduce UI models and prepare data in there
const getYearRepresentation = (date: string) => date.split('-')[0];

export const Album: React.FC<Props> = ({ imageUrl, name, artist, releaseYear }) => (
  <Wrapper>
    <ImageWrapper>
      <Image url={imageUrl} />
      <PlayIcon>
        <Icon type={IconType.Play} shade={IconShade.light} />
      </PlayIcon>
    </ImageWrapper>
    <Title>{name}</Title>
    <Meta>
      {artist}
      <MetaDot />
      {getYearRepresentation(releaseYear)}
    </Meta>
  </Wrapper>
);
