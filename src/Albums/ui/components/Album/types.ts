export type Props = {
  name: string;
  artist: string;
  releaseYear: string;
  imageUrl: string;
};
