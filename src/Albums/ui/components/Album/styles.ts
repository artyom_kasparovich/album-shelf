import styled from 'styled-components';

export const Wrapper = styled.article``;

export const Image = styled.div<{ url: string }>`
  padding-bottom: 100%;
  background: url(${({ url }) => url}) no-repeat;
  background-size: cover;
  border-radius: 1.3rem;
  box-shadow: 0.2rem 0.6rem 1rem rgb(0, 0, 0, 20%);
`;

export const ImageWrapper = styled.div`
  position: relative;
`;

export const PlayIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 0.6rem;
  right: 0.6rem;
  height: 3.5rem;
  width: 3.5rem;
  border-radius: 50%;
  border: 2px solid rgba(255, 255, 255, 0.5);
  background-color: rgba(0, 0, 0, 0.74);
`;

export const Title = styled.h4`
  margin-top: 1.6rem;
  font-size: 1.4rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.bold};
  line-height: 1.9rem;
  text-align: center;
`;

export const Meta = styled.p`
  text-align: center;
  font-size: 1rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.regular};
  line-height: 1.4rem;
  text-transform: uppercase;
  color: ${({ theme }) => theme.palette.info.secondary};
`;

export const MetaDot = styled.span`
  display: inline-block;
  vertical-align: 0.2rem;
  margin: 0 0.5rem;
  height: 0.32rem;
  width: 0.32rem;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.palette.info.secondary};
`;
