import React from 'react';
import { Album } from '../Album';
import { Wrapper } from './styles';
import type { Props } from './types';

export const AlbumList: React.FC<Props> = ({ albums }) => (
  <Wrapper>
    {albums.map((album) => (
      <Album
        key={album.id}
        artist={album.artists[0].name}
        imageUrl={album.cover}
        releaseYear={album.releaseDate}
        name={album.name}
      />
    ))}
  </Wrapper>
);
