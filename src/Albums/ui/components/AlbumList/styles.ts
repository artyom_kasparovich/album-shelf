import styled from 'styled-components';

export const Wrapper = styled.section`
  display: flex;
  flex-wrap: wrap;

  margin-top: -6.4vw;
  margin-left: -6.4vw;

  & > article {
    width: 40.4vw;
    margin-top: 6.4vw;
    margin-left: 6.4vw;
  }

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
    margin-top: -4.5vw;
    margin-left: -4.5vw;

    & > article {
      width: 27.3vw;
      margin-top: 4.5vw;
      margin-left: 4.5vw;
    }
  }

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    margin-top: -3.6rem;
    margin-left: -3.6rem;

    // browser desktop will show scrollbar on the right
    margin-right: -3.6rem;

    & > article {
      width: 21.5rem;
      margin-top: 3.6rem;
      margin-left: 3.6rem;
    }
  }
`;
