import AppTypes from 'AppTypes';

export type Props = {
  albums: AppTypes.Entities.Album[];
};
