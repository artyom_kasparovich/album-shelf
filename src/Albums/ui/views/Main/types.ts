export type Props = {
  query: string;
  sorting: string;
};

export type TotalAmountProps = {
  count: number;
};
