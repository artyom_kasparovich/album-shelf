import styled from 'styled-components';

export const ButtonRow = styled.div`
  margin-top: 2rem;
  text-align: center;

  & > button {
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const NoResult = styled.p`
  font-size: 1.4rem;
  color: ${({ theme }) => theme.palette.info.accent};
`;
