import React, { useState, useEffect } from 'react';
import { createPortal } from 'react-dom';
import { useFetchList } from '../../../hooks/useFetchList';
import { AlbumList } from '../../components/AlbumList';
import { ButtonRow, NoResult } from './styles';
import type { Props, TotalAmountProps } from './types';

const usePagination = (shouldBeReset: boolean) => {
  const [page, setPage] = useState(1);

  useEffect(() => {
    setPage(1);
  }, [shouldBeReset]);

  const nextPage = () => {
    setPage(page + 1);
  };

  return { page, nextPage };
};

const useInfinityLoading = <T,>(shouldBeReset: boolean, newData?: T) => {
  const [data, setData] = useState(newData);

  const mergePreviousAndNextData = () => {
    if (!newData) return;
    if (!Array.isArray(newData)) return;

    const arr = !Array.isArray(data) ? [] : data;
    setData([...arr, ...newData] as unknown as T);
  };

  useEffect(
    mergePreviousAndNextData,
    /**
     * Disable here intentionally
     */
    // eslint-disable-next-line
    [newData, setData]
  );

  useEffect(() => {
    if (!shouldBeReset) return;
    setData(undefined);
  }, [shouldBeReset, setData]);

  return {
    infinityData: data,
    isInfinityLoadingFinished: Array.isArray(newData) ? newData.length < 10 : false,
  };
};

const TotalAmount: React.FC<TotalAmountProps> = ({ count }) => {
  const text = `${count} albums listed`;
  const targetElement = document.getElementById('sidebar-note');

  if (!targetElement) return null;

  return createPortal(text, targetElement);
};

export const Main: React.FC<Props> = ({ query, sorting }) => {
  const [shouldBeReset, setShouldBeReset] = useState(false);
  const { page, nextPage } = usePagination(shouldBeReset);
  const { data, error, isError, isLoading } = useFetchList(page, query, sorting);
  const { infinityData, isInfinityLoadingFinished } = useInfinityLoading(shouldBeReset, data);

  useEffect(() => {
    setShouldBeReset(true);
  }, [query, sorting]);

  useEffect(() => {
    setShouldBeReset(false);
  }, [infinityData]);

  if (isError) return <div>{error}</div>;

  return (
    <>
      {infinityData && infinityData.length ? <TotalAmount count={infinityData.length} /> : null}

      {infinityData && !infinityData.length && isInfinityLoadingFinished ? (
        <NoResult>
          No results for <i>{query}</i> request. Please try another.
        </NoResult>
      ) : null}

      <AlbumList albums={infinityData ?? []} />

      {isInfinityLoadingFinished ? null : (
        <ButtonRow>
          <button type="button" disabled={isLoading} onClick={nextPage}>
            Load more
          </button>
        </ButtonRow>
      )}
    </>
  );
};
