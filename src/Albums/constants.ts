export enum SortingAlbums {
  name = 'name',
  releaseDate = 'releaseDate',
  totalTracks = 'totalTracks',
}

export const SortingAlbumsOptions = [
  {
    value: SortingAlbums.name,
    label: 'Name',
  },
  {
    value: SortingAlbums.releaseDate,
    label: 'Release date',
  },
  {
    value: SortingAlbums.totalTracks,
    label: 'Total tracks',
  },
];
