import 'styled-components';
import { Theme } from './ui/styles';

declare module 'styled-components' {
  export interface DefaultTheme extends Theme {}
}
