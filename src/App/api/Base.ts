import { AxiosInstance, AxiosRequestConfig } from 'axios';

const buildBaseURL = (endpoint: string, version: string) => `${endpoint}${version}`;

export interface BaseApiRequestConfig extends AxiosRequestConfig {
  version?: string;
}

export type BaseApiConstructorProps = {
  instance: AxiosInstance;
  config: {
    endpoint: string;
    version: string;
  };
};

export class Base {
  private readonly endpoint: string;

  private readonly version: string;

  readonly instance: AxiosInstance;

  constructor({ instance, config: { endpoint, version } }: BaseApiConstructorProps) {
    this.endpoint = endpoint;
    this.version = version;
    this.instance = instance;
  }

  private send<T>(url: string, options: BaseApiRequestConfig = {}): Promise<T> {
    const baseURL = options.baseURL ? options.baseURL : this.endpoint;
    const version = options.version ? options.version : this.version;

    return this.instance
      .request<T>({
        ...options,
        url,
        baseURL: buildBaseURL(baseURL, version),
      })
      .then((response) => response.data);
  }

  protected get<T>(url: string, options?: BaseApiRequestConfig): Promise<T> {
    return this.send<T>(url, {
      ...options,
      method: 'GET',
    });
  }

  protected post<T>(url: string, options?: BaseApiRequestConfig): Promise<T> {
    return this.send<T>(url, {
      ...options,
      method: 'POST',
    });
  }

  protected put<T>(url: string, options?: BaseApiRequestConfig): Promise<T> {
    return this.send<T>(url, {
      ...options,
      method: 'PUT',
    });
  }

  protected delete<T>(url: string, options?: BaseApiRequestConfig): Promise<T> {
    return this.send<T>(url, {
      ...options,
      method: 'DELETE',
    });
  }
}
