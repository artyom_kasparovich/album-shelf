import { Base } from 'App/api/Base';

export type PaginationRequestParams = {
  page: number;
};

export type SearchRequestParams = {
  query: string;
};

export type SortingRequestParams = {
  sorting: string;
};

export type FilteringRequestParams = {
  [x: string]: string | number;
};

export type ListRequestParams = PaginationRequestParams &
  SearchRequestParams &
  SortingRequestParams &
  FilteringRequestParams;

export abstract class List<E> extends Base {
  list(url: string, { page, query, sorting, ...filtering }: ListRequestParams): Promise<E[]> {
    return this.get(url, { params: { _page: page, q: query, _sort: sorting, ...filtering } });
  }
}
