import styled from 'styled-components';

export const Wrapper = styled.div`
  min-width: ${({ theme }) => theme.breakpoints.mobile};
  max-width: ${({ theme }) => theme.breakpoints.desktop};
  margin: 0 auto;

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: flex;
    height: 100vh;

    & > aside {
      width: 34.2%;
    }

    & > main {
      width: 65.8%;
    }
  }
`;

export const Box = styled.div`
  padding: 6.4vw;
  box-sizing: border-box;

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
    padding: 4.5vw;
  }

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 3.6rem;
  }
`;

export const SidebarNote = styled.p`
  margin-top: 1.7rem;
  font-size: 1.8rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.medium};
  line-height: 2.5rem;
  color: ${({ theme }) => theme.palette.info.secondary};
`;

export const Title = styled.h1`
  font-size: 4.3rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.extraBold};
  line-height: 5.9rem;
  letter-spacing: -0.05em;
  color: ${({ theme }) => theme.palette.info.accent};
`;

export const Sidebar = styled.aside`
  & > div > div {
    margin-top: 1rem;
  }

  ${Box} {
    @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      display: flex;
      flex-direction: column;
      flex: 1;
      height: 100%;

      & > ${SidebarNote} {
        margin-top: auto;
      }
    }
  }
`;

export const Content = styled.main`
  background-color: ${({ theme }) => theme.palette.bg.secondary};

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    height: 100%;
    overflow: auto;
  }
`;
