import React, { useState, useEffect } from 'react';
import { Accordion, TitleHR, Search, OptionsSelector } from 'App/ui/components';
import { Main as AlbumsMain } from 'Albums/ui/views/Main';
import { ArtistsFilter } from 'Artists/ui/views/Filter';
import { SortingAlbumsOptions, SortingAlbums } from 'Albums/constants';
import { Content, Sidebar, Wrapper, Box, SidebarNote, Title } from './styles';

export const Layout: React.FC = () => {
  const [search, setSearch] = useState('');
  const [sorting, setSorting] = useState<SortingAlbums>(SortingAlbums.name);
  const [artist, setArtist] = useState<string | null>(null);
  const query = artist ?? search;

  const resetArtistWhenSearchChanged = () => {
    setArtist(null);
  };
  useEffect(resetArtistWhenSearchChanged, [search]);

  const resetSearchWhenArtistChanged = () => {
    if (!artist) return;
    setSearch('');
  };
  useEffect(resetSearchWhenArtistChanged, [artist]);

  return (
    <Wrapper>
      <Sidebar>
        <Box>
          <Title>Album Shelf</Title>
          <Accordion title="Filters">
            {() => (
              <>
                <TitleHR title="Artist" />
                <ArtistsFilter value={artist} onArtistChosen={setArtist} />
                <TitleHR title="Search" />
                <Search value={search} onChange={setSearch} />
              </>
            )}
          </Accordion>

          <Accordion title="Sorting">
            {({ closeAccordeon }) => (
              <OptionsSelector
                options={SortingAlbumsOptions}
                value={sorting}
                onChangeValue={(val) => {
                  closeAccordeon();
                  setSorting(val);
                }}
              />
            )}
          </Accordion>

          <SidebarNote id="sidebar-note" />
        </Box>
      </Sidebar>

      <Content>
        <Box>
          <AlbumsMain query={query} sorting={sorting} />
        </Box>
      </Content>
    </Wrapper>
  );
};
