import styled from 'styled-components';
import { IconShade } from './constants';

export const SVG = styled.svg<{ shade: IconShade }>`
  width: 2.4rem;
  height: 2.4rem;
  fill: ${({ theme, shade }) =>
    shade === IconShade.light ? theme.palette.info.light : theme.palette.info.accentLight};
`;
