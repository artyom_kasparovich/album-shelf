import React from 'react';
import { SVG } from './styles';
import type { Props } from './types';

export * from './constants';

export const Icon: React.FC<Props> = ({ type, shade }) => (
  <SVG shade={shade}>
    <use xlinkHref={`#svg-${type}`} />
  </SVG>
);
