export enum IconType {
  Check = 'check',
  Clear = 'clear',
  Cross = 'cross',
  ExpandArrow = 'expandArrow',
  Play = 'play',
  Search = 'search',
}

export enum IconShade {
  light = 'light',
  accent = 'accent',
}
