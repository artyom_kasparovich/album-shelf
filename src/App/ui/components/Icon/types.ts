import { IconType, IconShade } from './constants';

export type Props = {
  type: IconType;
  shade: IconShade;
};
