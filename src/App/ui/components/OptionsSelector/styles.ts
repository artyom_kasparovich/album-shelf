import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  margin-top: 1.5rem;
  overflow: hidden;
  border-radius: 0.5rem;
  background-color: ${({ theme }) => theme.palette.bg.light};
`;

export const List = styled.ul`
  list-style-type: none;
`;

const active = css`
  color: ${({ theme }) => theme.palette.info.light};
  background-color: ${({ theme }) => theme.palette.info.accent};
`;

export const ListItem = styled.li<{ isActive: boolean }>`
  display: flex;
  align-items: center;
  padding: 0 1.4rem;
  line-height: 4.4rem;
  font-size: 1.8rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.bold};
  color: ${({ theme }) => theme.palette.info.accent};

  &:hover {
    ${active};
  }

  ${({ isActive }) => isActive && active};
`;

export const CircleIcon = styled.span`
  display: inline-block;
  width: 2rem;
  height: 2rem;
  opacity: 0.2;
  border-radius: 50%;
  border: 2px solid ${({ theme }) => theme.palette.info.secondary};
  box-sizing: border-box;
`;

export const ListItemText = styled.span`
  margin-left: 1.1rem;
`;
