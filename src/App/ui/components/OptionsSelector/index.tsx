import React from 'react';
import { Icon, IconType, IconShade } from 'App/ui/components/Icon';
import { Wrapper, List, ListItem, ListItemText, CircleIcon } from './styles';
import type { Props } from './models';

export const OptionsSelector = <T, O extends { value: T; label: string }>({
  options,
  value,
  onChangeValue,
}: Props<T, O>) => (
  <Wrapper>
    <List>
      {options.map((option) => {
        const isActive = value === option.value;

        return (
          <ListItem
            key={option.label}
            isActive={isActive}
            onClick={() => {
              onChangeValue(option.value);
            }}
          >
            {isActive ? <Icon type={IconType.Check} shade={IconShade.light} /> : <CircleIcon />}
            <ListItemText>{option.label}</ListItemText>
          </ListItem>
        );
      })}
    </List>
  </Wrapper>
);
