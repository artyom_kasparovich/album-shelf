export type Props<T, O extends { value: T; label: string }> = {
  options: O[];
  value: T;
  onChangeValue: (value: T) => void;
};
