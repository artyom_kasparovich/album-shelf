import styled from 'styled-components';

export const Title = styled.div`
  margin-top: 2rem;
  margin-bottom: 0.8rem;
  overflow: hidden;
`;

export const Text = styled.span`
  position: relative;
  font-size: 1.4rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.bold};
  line-height: 1.9rem;
  color: ${({ theme }) => theme.palette.info.accentLight};
  text-transform: uppercase;

  &:after {
    content: '';
    position: absolute;
    width: 100vw;
    left: 100%;
    top: 50%;
    margin-left: 0.5rem;
    height: 1px;
    background-color: ${({ theme }) => theme.palette.info.accentLight};
  }
`;
