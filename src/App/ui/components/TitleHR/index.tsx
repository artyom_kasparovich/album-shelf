import React from 'react';
import { Title, Text } from './styles';
import type { Props } from './types';

export const TitleHR: React.FC<Props> = ({ title }) => (
  <Title>
    <Text>{title}</Text>
  </Title>
);
