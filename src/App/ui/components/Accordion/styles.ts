import styled, { css } from 'styled-components';

export const Title = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const TitleText = styled.span`
  font-size: 1.8rem;
  line-height: 2.4rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.bold};
  color: ${({ theme }) => theme.palette.info.accent};
`;

export const TitleIcon = styled.span`
  cursor: pointer;
`;

export const Wrapper = styled.div<{ isOpen: boolean }>`
  padding: 0.9rem 1.4rem 1rem;
  border-radius: 0.9rem;
  background-color: ${({ theme }) => theme.palette.bg.accent};

  padding-bottom: ${({ isOpen }) => (isOpen ? '0.6rem' : '1.4rem')};
`;

const hidden = css`
  opacity: 0;
  max-height: 0;
  overflow: hidden;
  pagging: 0;
  margin: 0;
`;

export const Content = styled.div<{ isHidden: boolean }>`
  margin: 0 -0.8rem;
  max-height: 1000rem;
  transition: all 0.15s ease-in-out;
  ${({ isHidden }) => isHidden && hidden};
`;
