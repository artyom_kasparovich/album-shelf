import React, { useState } from 'react';
import { Icon, IconType, IconShade } from 'App/ui/components/Icon';
import { Wrapper, Title, Content, TitleText, TitleIcon } from './styles';
import type { Props } from './types';

export const Accordion: React.FC<Props> = ({ title, children }) => {
  const [isOpen, setOpen] = useState(false);
  const toggle = () => setOpen(!isOpen);
  const iconType = isOpen ? IconType.Cross : IconType.ExpandArrow;

  const closeAccordeon = () => {
    setOpen(false);
  };

  return (
    <Wrapper isOpen={isOpen}>
      <Title>
        <TitleText>{title}</TitleText>
        <TitleIcon onClick={toggle}>
          <Icon type={iconType} shade={IconShade.accent} />
        </TitleIcon>
      </Title>
      <Content isHidden={!isOpen}>{typeof children === 'function' ? children({ closeAccordeon }) : null}</Content>
    </Wrapper>
  );
};
