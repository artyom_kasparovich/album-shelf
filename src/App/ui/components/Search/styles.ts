import styled from 'styled-components';

export const Wrapper = styled.div`
  position: relative;
  display: flex;
`;

export const InputWrapper = styled.div`
  display: flex;
  align-items: center;
  width: calc(100% - 5.8rem);
  height: 4.4rem;

  box-sizing: border-box;
  border-radius: 0.5rem;
  background: ${({ theme }) => theme.palette.bg.light};
  border: 2px solid ${({ theme }) => theme.palette.info.accentBright};
`;

export const Input = styled.input`
  outline: none;
  border: none;
  backgound: transparent;
  min-width: 0;

  flex: 1;
  font-weight: ${({ theme }) => theme.font.primary.weight.medium};
  font-size: 1.8rem;
  color: ${({ theme }) => theme.palette.info.accent};
`;

export const IconWrapper = styled.span`
  margin: 0 0.9rem 0 1.4rem;
`;

export const ClearBtn = styled.button`
  border: none;
  outline: none;

  display: flex;
  align-items: center;
  justify-content: center;
  width: 5.2rem;
  margin-left: 0.6rem;
  background: ${({ theme }) => theme.palette.info.accent};
  border-radius: 0.5rem;
`;
