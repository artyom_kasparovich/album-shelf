import React from 'react';
import { IconType, IconShade, Icon } from 'App/ui/components/Icon';
import { Wrapper, Input, InputWrapper, IconWrapper, ClearBtn } from './styles';
import { Props } from './types';

export const Search: React.FC<Props> = ({ value, onChange }) => {
  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    onChange(e.target.value);
  };

  const handleClear = () => {
    onChange('');
  };

  return (
    <Wrapper>
      <InputWrapper>
        <IconWrapper>
          <Icon type={IconType.Search} shade={IconShade.accent} />
        </IconWrapper>

        <Input type="text" value={value} onChange={handleChange} />
      </InputWrapper>

      <ClearBtn type="button" onClick={handleClear}>
        <Icon type={IconType.Clear} shade={IconShade.light} />
      </ClearBtn>
    </Wrapper>
  );
};
