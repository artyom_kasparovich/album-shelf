import { createGlobalStyle } from 'styled-components';
import { reset } from 'styled-reset';
import { base } from './base';

export * from './theme';

export const GlobalStyles = createGlobalStyle`
    ${reset}
    ${base}
`;
