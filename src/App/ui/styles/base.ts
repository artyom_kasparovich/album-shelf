import { css } from 'styled-components';

export const base = css`
  :root {
    font-size: 62.5%;
  }

  html {
    font-family: ${({ theme }) => theme.font.primary.family};
    font-weight: ${({ theme }) => theme.font.primary.weight.regular};
  }

  input,
  select,
  textaria,
  button {
    font-family: inherit;
  }
`;
