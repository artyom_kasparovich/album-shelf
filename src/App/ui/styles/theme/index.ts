import { font } from './font';
import { palette } from './palette';
import { Theme } from './types';

export * from './types';

/**
 * Note:: In real project theme tokens should be discussed with a designer.
 * Especially if UI will be multiplied on several systems
 */
export const theme: Theme = {
  font,
  palette,
  breakpoints: {
    mobile: '320px',
    tablet: '768px',
    desktop: '1200px',
  },
};
