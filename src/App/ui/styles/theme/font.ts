export const font = {
  primary: {
    family: 'Manrope, sans-serif',
    weight: {
      regular: 400,
      medium: 500,
      bold: 700,
      extraBold: 800,
    },
  },
};
