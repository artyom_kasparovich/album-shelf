type ThemeFont = {
  primary: {
    family: string;
    weight: {
      regular: number;
      medium: number;
      bold: number;
      extraBold: number;
    };
  };
};

type ThemePalette = {
  info: {
    // primary: string;
    secondary: string;
    accent: string;
    accentLight: string;
    accentBright: string;
    light: string;
  };
  bg: {
    accent: string;
    light: string;
    secondary: string;
  };
};

type ThemeBreakPoints = {
  mobile: string;
  tablet: string;
  desktop: string;
};

export interface Theme {
  font: ThemeFont;
  palette: ThemePalette;
  breakpoints: ThemeBreakPoints;
}
