const blue = {
  normal: '#8EC3E4',
  light: '#D8EAF1',
  dark: '#223C4C',
  bright: '#39A4E8',
};

const gray = {
  normal: '#B2B8BC',
  light: '#EAEBEC',
};

const white = {
  normal: '#F7F8F8',
};

export const palette = {
  info: {
    accent: blue.dark,
    accentLight: blue.normal,
    accentBright: blue.bright,
    light: white.normal,
    secondary: gray.normal,
  },
  bg: {
    accent: blue.light,
    light: white.normal,
    secondary: gray.light,
  },
};
