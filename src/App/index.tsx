import React from 'react';
import ReactDOM from 'react-dom';
import type { Config } from 'config';
import axios from 'axios';
import { ThemeProvider } from 'styled-components';
import { APIContext } from './context';
import { Layout } from './ui/views/Layout';
import { theme, GlobalStyles } from './ui/styles';

/**
 * App might be portable and be initiated as a widget in a page
 */
export const initApp = (config: Config) => {
  /**
   * Here will go bootstrap of the app, services initialization, injecting dependencies and so on
   */
  const { appId, api } = config;
  const apiContextValue = {
    instance: axios.create(),
    ...api,
  };

  ReactDOM.render(
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <APIContext.Provider value={apiContextValue}>
          <Layout />
        </APIContext.Provider>
      </ThemeProvider>
    </React.StrictMode>,
    document.getElementById(appId)
  );
};
