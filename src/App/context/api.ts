import React from 'react';
import { AxiosInstance } from 'axios';

type ContextAPI = {
  version: string;
  endpoint: string;
  instance: AxiosInstance;
};

export const APIContext = React.createContext<ContextAPI>({} as ContextAPI);
