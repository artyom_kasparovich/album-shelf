import { useState, useEffect, useRef, useCallback } from 'react';
import { debounce } from 'lodash';

export const useFetch = <R>(requestFn: () => Promise<R>, isDelayed = false) => {
  const [data, setData] = useState<R>();
  const [error, setError] = useState();
  const [isLoading, setLoading] = useState(false);

  const handleRequest = useCallback(() => {
    setLoading(true);

    requestFn()
      .then(setData)
      .catch(setError)
      .finally(() => {
        setLoading(false);
      });
  }, [requestFn]);

  const delayedRequest = useRef(debounce((fn) => fn(), 300));

  useEffect(() => {
    if (isDelayed) {
      delayedRequest.current(handleRequest);
    } else {
      handleRequest();
    }
  }, [handleRequest, isDelayed]);

  return {
    data,
    error,
    isLoading,
    isError: !!error,
  };
};
