import { useContext } from 'react';
import { APIContext } from 'App/context';

export const useAPIContext = () => useContext(APIContext);
