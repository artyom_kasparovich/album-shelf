import { useMemo } from 'react';
import { BaseApiConstructorProps } from 'App/api/Base';
import { useAPIContext } from 'App/hooks';

interface Constructable<T> {
  new (args: BaseApiConstructorProps): T;
}

export const useAPIInitialization = <A>(API: Constructable<A>) => {
  const context = useAPIContext();
  const api = useMemo(() => new API({ instance: context.instance, config: { ...context } }), [context, API]);

  return api;
};
