import { BaseApiConstructorProps } from 'App/api/Base';
import { List } from 'App/api/List';
import { Artist } from './types';

export class Artists extends List<Artist> {
  private url: string;

  constructor(props: BaseApiConstructorProps) {
    super(props);
    this.url = '/artists';
  }

  listOfMatureArtists() {
    return this.list(this.url, { page: 1, query: '', sorting: '', albums_gte: 2 });
  }
}
