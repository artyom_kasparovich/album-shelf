import React from 'react';
import { useFetchListOfMatureArtists } from '../../../hooks/useFetchList';
import { Wrapper, Item } from './styles';
import type { Props } from './types';

const ALL = {
  id: null,
  name: 'All',
};

export const ArtistsFilter: React.FC<Props> = ({ value, onArtistChosen }) => {
  const { data, error, isError, isLoading } = useFetchListOfMatureArtists();
  const artists = [ALL, ...(data ?? [])];

  if (isError) return <div>{error}</div>;

  return (
    <Wrapper>
      {isLoading ? <div>loading...</div> : null}

      {artists.map((artist) => (
        <Item
          isActive={value === artist.id}
          key={artist.id}
          onClick={() => {
            onArtistChosen(artist.id);
          }}
        >
          {artist.name}
        </Item>
      ))}
    </Wrapper>
  );
};
