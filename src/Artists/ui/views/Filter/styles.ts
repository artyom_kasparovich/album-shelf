import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0.7rem;
  border-radius: 0.5rem;
  background-color: ${({ theme }) => theme.palette.bg.light};
`;

const itemActive = css`
  color: ${({ theme }) => theme.palette.info.light};
  background-color: ${({ theme }) => theme.palette.info.accent};
`;

export const Item = styled.span<{ isActive: boolean }>`
  margin: 0.4rem;
  padding: 0.3rem 1rem;
  border: ${({ theme }) => theme.palette.info.accent} solid 1px;
  border-radius: 7.2rem;

  font-size: 1.6rem;
  font-weight: ${({ theme }) => theme.font.primary.weight.medium};
  color: ${({ theme }) => theme.palette.info.accent};
  line-height: 2.2rem;
  cursor: pointer;

  transition: all 0.15s;

  &:hover {
    ${itemActive};
  }

  ${({ isActive }) => isActive && itemActive};
`;
