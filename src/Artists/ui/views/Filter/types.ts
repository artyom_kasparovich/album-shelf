export type Props = {
  value: string | null;
  onArtistChosen: (value: string | null) => void;
};
