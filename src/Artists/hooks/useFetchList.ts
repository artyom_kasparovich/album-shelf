import { useMemo } from 'react';
import { useFetch, useAPIInitialization } from 'App/hooks';
import { Artists } from '../api';

export const useFetchListOfMatureArtists = () => {
  const artists = useAPIInitialization(Artists);
  const fetchListOfMatureArtists = useMemo(() => () => artists.listOfMatureArtists(), [artists]);
  const response = useFetch(fetchListOfMatureArtists);

  return response;
};
