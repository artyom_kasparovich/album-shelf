/**
 * Here we can implement configuration system
 */
export const config = {
  appId: 'root',
  api: {
    endpoint: 'http://localhost:4000',
    version: '',
  },
};

export type Config = typeof config;
