# Why we are doing this

Dear Front-end developer candidate, welcome to our coding challenge! This document takes the format of a ticket that looks similar to ones you might be working on at our company. Of course, the scope of this challenge ticket is slightly wider than a ticket in production would be, in order to best serve the goals of this exercise. We want to…

- see that you successfully interpret the attached design artefacts into a functional piece of web UI.
- get an understanding for how you think about programming in the most concrete sense, by looking at this specific output.
- clear written communication in the release notes (see last Acceptance Criteria).

Treat this as if this task has been given to you by the product owner and it's up to you to deliver the best possible UI/UX to the user. Asking questions is highly encouraged.

# Current Status

Usually a ticket like this would describe the current state of affairs, problems and functionality gaps. In this case, you're starting out with an already implemented backend. We did a terrible job implementing it however, and it is a bit slow. 🙈

More information about how to use the API you will find in the GitHub repository of the assignment.

But front-end wise, the starting point is completely blank (besides some boiler-plate code).

# Acceptance Criteria

- [ ] Create a new interface for browsing a collection of albums as per the attached design artefacts.
  - [ ] Album artwork is rendered in a square format.
  - [ ] The typeface is "Manrope" - [it's available on Google Fonts](https://fonts.google.com/specimen/Manrope)
- [ ] Data for the interface is loaded via the API provided in the Github repo
- [ ] Sorting of the albums is possible by the album name, release date and total tracks
- [ ] When choosing a sorting variable, the sorting section collapses automatically
- [ ] The interface scales nicely from a 320px wide viewport up to 1200px
- [ ] It is possible to browse through the whole catalogue of albums
- [ ] Once you're happy to share your work, please write the 'release notes' for this feature. The format here is free form.

## Bonus

- [ ] Filtering by text input search.
- [ ] Filtering by artist.
  - [ ] filtering by only those artists who have two or more albums.

# Design Artefacts

Never-mind the David Bowie centric content: We simply found a nice data source with his "Golden Years" records.
